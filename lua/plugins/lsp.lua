local function lsp_highlight_document(client)
	-- Set autocommands conditional on server_capabilities
	if client.server_capabilities.document_highlight then
		vim.api.nvim_exec(
			[[
      augroup lsp_document_highlight
        autocmd! * <buffer>
        autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
        autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
      augroup END
    ]],
			false
		)
	end
end

local function on_attach(client, bufnr)
	if client.name == "pylsp" then
		client.server_capabilities.documentFormattingProvider = false
	end
	if client.name == "ts_ls" then
		client.server_capabilities.documentFormattingProvider = false
	end
	if client.name == "volar" then
		client.server_capabilities.documentFormattingProvider = false
	end
	if client.name == "gopls" then
		client.server_capabilities.documentFormattingProvider = false
	end
	if client.name == "eslint" then
		client.server_capabilities.documentFormattingProvider = false
	end
	if client.name == "lua_ls" then
		client.server_capabilities.documentFormattingProvider = false
	end
	lsp_highlight_document(client)
end

return {
	{
		"williamboman/mason.nvim",
		config = function()
			require("mason").setup()
		end,
	},
	{
		"neovim/nvim-lspconfig",
		config = function()
			vim.api.nvim_create_autocmd("LspAttach", {
				group = vim.api.nvim_create_augroup("UserLspConfig", {}),
				callback = function(ev)
					-- Enable completion triggered by <c-x><c-o>
					vim.bo[ev.buf].omnifunc = "v:lua.vim.lsp.omnifunc"

					-- Buffer local mappings.
					-- See `:help vim.lsp.*` for documentation on any of the below functions
					local opts = { buffer = ev.buf }
					vim.keymap.set("n", "gD", vim.lsp.buf.declaration, opts)
					vim.keymap.set("n", "gd", vim.lsp.buf.definition, opts)
					vim.keymap.set("n", "K", vim.lsp.buf.hover, opts)
					vim.keymap.set("n", "gi", vim.lsp.buf.implementation, opts)
					vim.keymap.set("n", "<C-k>", vim.lsp.buf.signature_help, opts)
					vim.keymap.set("n", "<leader>wa", vim.lsp.buf.add_workspace_folder, opts)
					vim.keymap.set("n", "<leader>wr", vim.lsp.buf.remove_workspace_folder, opts)
					vim.keymap.set("n", "<leader>wl", function()
						print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
					end, opts)
					vim.keymap.set("n", "<leader>D", vim.lsp.buf.type_definition, opts)
					vim.keymap.set("n", "<leader>rn", vim.lsp.buf.rename, opts)
					vim.keymap.set({ "n", "v" }, "<leader>ca", vim.lsp.buf.code_action, opts)
					vim.keymap.set("n", "gr", vim.lsp.buf.references, opts)
					vim.keymap.set("n", "<leader>F", function()
						vim.lsp.buf.format({ async = true })
					end, opts)
				end,
			})
		end,
	},
	{
		"williamboman/mason-lspconfig.nvim",
		config = function()
			local mason_lspconfig = require("mason-lspconfig")
			local lspconfig = require("lspconfig")
			local capabilities = require("blink.cmp").get_lsp_capabilities()
			local mason_registry = require("mason-registry")
			mason_lspconfig.setup({

				ensure_installed = {
					"eslint",
					"jsonls",
					--[[ "pyright", ]]
					"pylsp",
					-- DOCS pylsp
					-- :PylspInstall pylsp-rope pylsp-mypy pyls-isort python-lsp-black python-lsp-ruff
					-- source https://github.com/williamboman/mason-lspconfig.nvim/blob/main/lua/mason-lspconfig/server_configurations/pylsp/README.md
					--[[ "jedi_language_server", ]]
					--[[ "pylyzer", ]]
					"dockerls",
					"graphql",
					"ts_ls",
					"tailwindcss",
					"volar",
					"html",
					"lua_ls",
					"rust_analyzer",
					"marksman",
					"ruff",
					"gopls",
				},

				-- Whether servers that are set up (via lspconfig) should be automatically installed if they're not already installed.
				-- This setting has no relation with the `ensure_installed` setting.
				-- Can either be:
				--   - false: Servers are not automatically installed.
				--   - true: All servers set up via lspconfig are automatically installed.
				--   - { exclude: string[] }: All servers set up via lspconfig, except the ones provided in the list, are automatically installed.
				--       Example: automatic_installation = { exclude = { "rust_analyzer", "solargraph" } }
				--[[ automatic_installation = false, ]]
			})
			--[[ https://www.reddit.com/r/neovim/comments/xbbfj2/cant_get_masonlspconfig_to_work_properly/ ]]
			mason_lspconfig.setup_handlers({

				-- This is a default handler that will be called for each installed server (also for new servers that are installed during a session)
				function(server_name)
					if server_name == "pylsp" then
						lspconfig[server_name].setup({
							on_attach = on_attach,
							capabilities = capabilities,
							settings = {
								pylsp = {
									plugins = {
										rope = { enabled = true },
										rope_completion = { enabled = true, eager = false },
										rope_autoimport = {
											enabled = true,
											eager = false,
										},
										jedi_completion = {
											enabled = true,
											cache_for = { "django", "rest_framework", "fastapi" },
										},
										jedi_definition = {
											enabled = true,
											follow_imports = true,
											follow_builtin_imports = true,
										},
										jedi_hover = { enabled = true },
										jedi_references = { enabled = true },
										jedi_signature_help = { enabled = true },
										jedi_symbols = {
											enabled = true,
											all_scopes = true,
											include_import_symbols = true,
										},
										preload = {
											enabled = true,
											modules = { "django", "rest_framework", "fastapi" },
										},
										mccabe = { enabled = false },
										pycodestyle = { enabled = false },
										pyflakes = { enabled = false },
										flake8 = { enabled = false },
										mypy = { enabled = false },
										isort = { enabled = false },
										spyder = { enabled = false },
										memestra = { enabled = false },
										yapf = { enabled = false },
										pylint = { enabled = false },
									},
								},
							},
						})
					elseif server_name == "lua_ls" then
						lspconfig[server_name].setup({
							on_attach = on_attach,
							capabilities = capabilities,
							settings = {
								Lua = {
									diagnostics = {
										globals = { "vim" },
									},
								},
							},
						})
					elseif server_name == "ts_ls" then
						local vue_language_server_path =
							mason_registry.get_package("vue-language-server"):get_install_path()
						lspconfig[server_name].setup({
							on_attach = on_attach,
							capabilities = capabilities,
							init_options = {
								plugins = {
									{
										name = "@vue/typescript-plugin",
										location = vue_language_server_path,
										languages = { "vue" },
									},
								},
							},
							filetypes = { "typescript", "javascript", "javascriptreact", "typescriptreact" },
						})
					elseif server_name == "volar" then
						--	https://github.com/vuejs/language-tools
						lspconfig[server_name].setup({
							init_options = {
								vue = {
									hybridMode = false,
								},
							},
							on_attach = on_attach,
							capabilities = capabilities,
							filetypes = { "vue" },
						})
					else
						lspconfig[server_name].setup({
							on_attach = on_attach,
							capabilities = capabilities,
						})
					end
				end,
			})
		end,
	},
	dependencies = {
		"williamboman/mason.nvim",
		"neovim/nvim-lspconfig",
	},
}
