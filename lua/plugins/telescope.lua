return {
	{
		"nvim-telescope/telescope-fzf-native.nvim",
		build = "cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build",
	},
	{
		"nvim-telescope/telescope.nvim",
		dependencies = {
			"nvim-telescope/telescope-fzf-native.nvim",
			"nvim-telescope/telescope-media-files.nvim",
			"lpoto/telescope-docker.nvim",
			"nvim-telescope/telescope-ui-select.nvim",
		},
		config = function()
			local actions = require("telescope.actions")
			local telescope = require("telescope")

			-- local opts = { noremap = true, silent = true }
			local builtin = require("telescope.builtin")
			-- try to use default keybindings
			-- vim.keymap.set("n", "<leader>f", "<cmd>Telescope git_files hidden=true<cr>", opts)
			-- vim.keymap.set("n", "<c-g>", "<cmd>Telescope live_grep hidden=true<cr>", opts)

			-- try default keybindings
			vim.keymap.set("n", "<leader>ff", builtin.find_files, {})
			vim.keymap.set("n", "<leader>fg", builtin.live_grep, {})
			vim.keymap.set("n", "<leader>fb", builtin.buffers, {})
			vim.keymap.set("n", "<leader>fh", builtin.help_tags, {})

			telescope.setup({
				defaults = {

					file_ignore_patterns = {
						"vendor/*",
						"%.lock",
						"__pycache__/*",
						"%.sqlite3",
						"%.ipynb",
						"node_modules/*",
						"%.svg",
						"%.otf",
						"%.ttf",
						".git/",
						"%.webp",
						".dart_tool/",
						".github/",
						".gradle/",
						".idea/",
						".settings/",
						".vscode/",
						"__pycache__/",
						"build/",
						"env/",
						"gradle/",
						"node_modules/",
						"target/",
						"%.pdb",
						"%.dll",
						"%.class",
						"%.exe",
						"%.cache",
						"%.ico",
						"%.pdf",
						"%.dylib",
						"%.jar",
						"%.docx",
						"%.met",
						"smalljre_*/*",
						".vale/",
						"static",
					},
					prompt_prefix = " ",
					selection_caret = " ",
					path_display = { "smart" },

					mappings = {
						i = {
							["<C-n>"] = actions.cycle_history_next,
							["<C-p>"] = actions.cycle_history_prev,

							["<C-j>"] = actions.move_selection_next,
							["<C-k>"] = actions.move_selection_previous,

							["<C-c>"] = actions.close,

							["<Down>"] = actions.move_selection_next,
							["<Up>"] = actions.move_selection_previous,

							["<CR>"] = actions.select_default,
							["<C-x>"] = actions.select_horizontal,
							["<C-v>"] = actions.select_vertical,
							["<C-t>"] = actions.select_tab,

							["<C-u>"] = actions.preview_scrolling_up,
							["<C-d>"] = actions.preview_scrolling_down,

							["<PageUp>"] = actions.results_scrolling_up,
							["<PageDown>"] = actions.results_scrolling_down,

							["<Tab>"] = actions.toggle_selection + actions.move_selection_worse,
							["<S-Tab>"] = actions.toggle_selection + actions.move_selection_better,
							["<C-q>"] = actions.send_to_qflist + actions.open_qflist,
							["<M-q>"] = actions.send_selected_to_qflist + actions.open_qflist,
							["<C-l>"] = actions.complete_tag,
							["<C-_>"] = actions.which_key, -- keys from pressing <C-/>
						},

						n = {
							["<esc>"] = actions.close,
							["<CR>"] = actions.select_default,
							["<C-x>"] = actions.select_horizontal,
							["<C-v>"] = actions.select_vertical,
							["<C-t>"] = actions.select_tab,

							["<Tab>"] = actions.toggle_selection + actions.move_selection_worse,
							["<S-Tab>"] = actions.toggle_selection + actions.move_selection_better,
							["<C-q>"] = actions.send_to_qflist + actions.open_qflist,
							["<M-q>"] = actions.send_selected_to_qflist + actions.open_qflist,

							["j"] = actions.move_selection_next,
							["k"] = actions.move_selection_previous,
							["H"] = actions.move_to_top,
							["M"] = actions.move_to_middle,
							["L"] = actions.move_to_bottom,

							["<Down>"] = actions.move_selection_next,
							["<Up>"] = actions.move_selection_previous,
							["gg"] = actions.move_to_top,
							["G"] = actions.move_to_bottom,

							["<C-u>"] = actions.preview_scrolling_up,
							["<C-d>"] = actions.preview_scrolling_down,

							["<PageUp>"] = actions.results_scrolling_up,
							["<PageDown>"] = actions.results_scrolling_down,

							["?"] = actions.which_key,
						},
					},
				},
				pickers = {
					-- Default configuration for builtin pickers goes here:
					-- picker_name = {
					--   picker_config_key = value,
					--   ...
					-- }
					-- Now the picker_config_key will be applied every time you call this
					-- builtin picker
					--

					find_files = {
						hidden = true,
					},
				},
				extensions = {
					fzf = {
						fuzzy = true, -- false will only do exact matching
						override_generic_sorter = true, -- override the generic sorter
						override_file_sorter = true, -- override the file sorter
						case_mode = "smart_case", -- or "ignore_case" or "respect_case"
						-- the default case_mode is "smart_case"
					},
					media_files = {
						-- filetypes whitelist
						-- defaults to {"png", "jpg", "mp4", "webm", "pdf"}
						filetypes = { "png", "webp", "jpg", "jpeg" },
						find_cmd = "rg", -- find command (defaults to `fd`)
					},
					docker = {
						-- These are the default values
						-- theme = "ivy",
						binary = "docker", -- in case you want to use podman or something
						compose_binary = "docker compose",
						buildx_binary = "docker buildx",
						machine_binary = "docker-machine",
						log_level = vim.log.levels.INFO,
						init_term = "tabnew", -- "vsplit new", "split new", ...
						-- NOTE: init_term may also be a function that receives
						-- a command, a table of env. variables and cwd as input.
						-- This is intended only for advanced use, in case you want
						-- to send the env. and command to a tmux terminal or floaterm
						-- or something other than a built in terminal.
					},
					["ui-select"] = {
						require("telescope.themes").get_dropdown({}),
					},
				},
			})

			telescope.load_extension("fzf")
			telescope.load_extension("media_files")
			telescope.load_extension("docker")
			telescope.load_extension("ui-select")
			telescope.load_extension("luasnip")
		end,
	},
}
