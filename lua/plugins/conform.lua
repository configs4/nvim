-- install prettierd locally
-- https://github.com/fsouza/prettierd
-- npm install -g @fsouza/prettierd
--
-- prettier plugins
-- https://github.com/tailwindlabs/prettier-plugin-tailwindcss
-- npm install -g prettier-plugin-tailwindcss
return {
	"stevearc/conform.nvim",
	event = { "BufWritePre" },
	cmd = { "ConformInfo" },
	keys = {
		{
			-- Customize or remove this keymap to your liking
			"<leader>bf",
			function()
				require("conform").format({ async = true, lsp_fallback = true })
			end,
			mode = "",
			desc = "Format buffer",
		},
	},
	-- Everything in opts will be passed to setup()
	opts = {
		-- Define your formatters
		formatters_by_ft = {
			lua = { "stylua" },
			python = function(bufnr)
				if require("conform").get_formatter_info("ruff_format", bufnr).available then
					return { "isort", "ruff_format" }
				else
					return { "isort", "black" }
				end
			end,
			javascript = { "prettier" },
			typescript = { "prettier" },
			vue = { "prettier" },
			css = { "prettier" },
			scss = { "prettier" },
			less = { "prettier" },
			html = { "prettier" },
			json = { "prettier" },
			jsonc = { "prettier" },
			yaml = { "prettier" },
			markdown = { "prettier" },
			mdx = { "prettier" },
			go = { "goimports", "gofmt" },
		},
		-- Set up format-on-save
		format_on_save = { timeout_ms = 1500, lsp_fallback = true },
	},
	init = function()
		-- If you want the formatexpr, here is the place to set it
		vim.o.formatexpr = "v:lua.require'conform'.formatexpr()"
	end,
}
