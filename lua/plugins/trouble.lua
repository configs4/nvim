return {
	"folke/trouble.nvim",
	dependencies = "nvim-tree/nvim-web-devicons",
	-- dependencies = "kyazdani42/nvim-web-devicons",
	config = function()
		require("trouble").setup({
			-- your configuration comes here
			-- or leave it empty to the default settings
			-- refer to the configuration section below
		})
	end,
}
