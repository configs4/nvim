return {
	"ellisonleao/gruvbox.nvim",
	priority = 1000,
	lazy = false,
	config = function()
		vim.cmd([[colorscheme gruvbox]])
		vim.cmd([[set background=dark]]) -- or "light" for light mode
	end,
	dependencies = {
		-- "kyazdani42/nvim-web-devicons",
		"nvim-tree/nvim-web-devicons",
		"NvChad/nvim-colorizer.lua",
	},
}
