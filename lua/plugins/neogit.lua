return {
	"NeogitOrg/neogit",
	dependencies = {
		"nvim-lua/plenary.nvim",
		"sindrets/diffview.nvim",
	},
	config = function()
		local opts = { noremap = true, silent = true }
		vim.keymap.set("n", "<leader>gs", ":Neogit<cr>", opts)
		require("neogit").setup()
	end,
}
